
/**
 * Created by Bat on 14/09/2014.
 */
(function() {
    angular.module('app', ['gridster', 'ui.bootstrap', 'ngRoute','ngDragDrop'])
        .config(['$routeProvider',
            function($routeProvider) {
                $routeProvider

                    .when('/dashboard', {
                        templateUrl: 'demo/dashboard/view.html',
                        controller: 'DashboardCtrl'
                    })

            }
        ])
        .controller('RootCtrl', function($scope) {

            $scope.$on('$locationChangeStart', function(e, next, current) {
                $scope.page = next.split('/').splice(-1);
                $scope.styleUrl = 'demo/' + $scope.page + '/style.css'
            });
        });
})();