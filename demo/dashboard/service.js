/**
 * Created by Bat on 14/09/2014.
 */
angular.module('app')

    .factory('tablesScv', ['$http', '$q', function ($http, $q) {
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        var getTablesSvc = function () {

            var deferred = $q.defer();
            $http({
                type: "GET",
                dataType: 'jsonp',
                contentType: "application/json; charset=utf-8",
                url: "http://localhost:8080/kasir/selectsank.php?table=tables"})
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (reason) {
                    deferred.reject(reason);
                });
            return deferred.promise;

        };
        var updateTablesSvc = function (save) {
            debugger
            var deferred = $q.defer();
            var xsrf = $.param(save);
            $http(

                {
                    method: 'POST',
                    url: "http://localhost:8080/kasir/sacuvajsank.php",
                    data: xsrf,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }

            )

            return deferred.promise;

        };
        var addTablesSvc = function (insert) {
            debugger
            var deferred = $q.defer();
            var xsrf = $.param(insert);
            $http(

                {
                    method: 'POST',
                    url: "http://localhost:8080/kasir/addsank.php",
                    data: xsrf,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }

            )
                .success(function (data) {
                    deferred.resolve(data);
                    debugger

                })
                .error(function (reason) {
                    deferred.reject(reason);
                });
            return deferred.promise;

        };
        var deleteTablesSvc = function (id) {
            debugger
            var deferred = $q.defer();
            var xsrf = $.param(id);
            $http(

                {
                    method: 'POST',
                    url: "http://localhost:8080/kasir/deletesank.php",
                    data: xsrf,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }

            )
                .success(function (data) {
                    deferred.resolve(data);
                    debugger

                })
                .error(function (reason) {
                    deferred.reject(reason);
                });
            return deferred.promise;

        };
        return{
            getTablesSvc: getTablesSvc,
            addTablesSvc: addTablesSvc,
            deleteTablesSvc: deleteTablesSvc,
            updateTablesSvc:updateTablesSvc

        };
    }])
