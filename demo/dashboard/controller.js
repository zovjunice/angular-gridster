angular.module('app')
    .controller('DashboardCtrl', ['$scope', '$timeout', 'tablesScv', '$http', '$rootScope',
        function ($scope, $timeout, tablesScv, $http, $rootScope) {

            $scope.list1 = {title: 'AngularJS - Drag Me'};
            $scope.list2 = {};


            $scope.gridsterOptions = {
                columns: 10, // the width of the grid, in columns
                pushing: true, // whether to push other items out of the way on move or resize
                floating: false, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
                width: 'auto', // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
                colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
                rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
                margins: [10, 10], // the pixel distance between each widget
                outerMargin: true, // whether margins apply to outer edges of the grid
                isMobile: false, // stacks the grid items if true
                mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
                mobileModeEnabled: true, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
                minColumns: 1, // the minimum columns the grid must have
                minRows: 1, // the minimum height of the grid, in rows
                maxRows: 100,
                defaultSizeX: 2, // the default width of a gridster item, if not specifed
                defaultSizeY: 1, // the default height of a gridster item, if not specified
                resizable: {
                    enabled: true,
                    handles: 'n, e, s, w, ne, se, sw, nw',
                    start: function (event, uiWidget, $element) {
                    }, // optional callback fired when resize is started,
                    resize: function (event, uiWidget, $element) {
                    }, // optional callback fired when item is resized,
                    stop: function (event, uiWidget, $element) {
                    } // optional callback fired when item is finished resizing
                },
                draggable: {
                    enabled: true, // whether dragging items is supported
                    handle: '.box-header', // optional selector for resize handle
                    start: function (event, uiWidget, $element) {
                    }, // optional callback fired when drag is started,
                    drag: function (event, uiWidget, $element) {
                    }, // optional callback fired when item is moved,
                    stop: function (event, uiWidget, $element) {
                    } // optional callback fired when item is finished dragging
                }
            };
            $scope.getTables = function () {

                return tablesScv.getTablesSvc().then(
                    function (data) {
                        debugger
                        $scope.dashboards = data.object_name;


                    },
                    function (resolve) {
                        return resolve;
                    }
                );


            };
            $scope.saveTables = function (save) {

                return tablesScv.updateTablesSvc(save);


            };
            $scope.addTables = function (insert) {

                return tablesScv.addTablesSvc(insert).then(
                    function (data) {

                        $scope.dashboards.push(insert);

                    },
                    function (resolve) {
                        return resolve;
                    }
                );


            };
            $scope.deleteTable = function (id) {

                return tablesScv.deleteTablesSvc(id).then(
                    function (data) {


                    },
                    function (resolve) {
                        return resolve;
                    }
                );


            };


            $scope.clear = function () {
                $scope.dashboards = [];
            };
            $scope.remove = function (widget) {
                $scope.deleteTable({table: 'tables', where: "id", what: widget.id});
                $scope.dashboards.splice($scope.dashboards.indexOf(widget), 1);
            };

            $scope.addWidget = function () {
                $scope.addTables({
                    table: 'tables',
                    name: "New Widget",
                    sizeX: 1,
                    sizeY: 1,
                    col: 3,
                    row: 0

                });

            };
            $scope.saveAll = function () {
                $scope.$watch('dashboards', function (newVal, oldVal) {

                    console.log(newVal);
                }, true);

            };



        }
    ])

    .controller('CustomWidgetCtrl', ['$scope', '$modal',
        function ($scope, $modal) {

            /*  $scope.remove = function (widget) {
             $rootScope.dashboard.widgets.splice($scope.dashboard.widgets.indexOf(widget), 1);
             };*/

            $scope.openSettings = function (widget) {
                $modal.open({
                    scope: $scope,
                    templateUrl: 'demo/dashboard/widget_settings.html',
                    controller: 'WidgetSettingsCtrl',
                    resolve: {
                        widget: function () {
                            return widget;
                        }
                    }
                });
            };
            function merge_options(obj1,obj2){
                var obj3 = {};
                for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
                for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
                return obj3;
            }
            var what={pamti:''};
            $scope.save = function () {

                $scope.$watch('widget', function (newVal, oldVal) {
                    debugger
                    if(what.pamti==="") {
                        what.pamti = newVal.id;
                    }
                    delete newVal.$$hashKey;
                    delete newVal.id;
                    newVal= merge_options(newVal,{where:'id',what:what.pamti,table:'tables'});
                    console.log(newVal);
                    $scope.saveTables(newVal);

                }, true);

            };


        }
    ])

    .controller('WidgetSettingsCtrl', ['$scope', '$timeout', '$rootScope', '$modalInstance', 'widget',
        function ($scope, $timeout, $rootScope, $modalInstance, widget) {
            $scope.widget = widget;

            $scope.form = {
                name: widget.name,
                sizeX: widget.sizeX,
                sizeY: widget.sizeY,
                col: widget.col,
                row: widget.row
            };


            $scope.dismiss = function () {
                $modalInstance.dismiss();
            };

            $scope.remove = function () {
                $scope.dashboard.widgets.splice($scope.dashboard.widgets.indexOf(widget), 1);
                $modalInstance.close();
            };

            $scope.submit = function () {
                angular.extend(widget, $scope.form);
                $modalInstance.close(widget);

            };


        }
    ])

// helper code
    .filter('object2Array', function () {
        return function (input) {
            var out = [];
            for (i in input) {
                out.push(input[i]);
            }
            return out;
        }
    });
